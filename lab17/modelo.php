<?php

function conectar() {
    $mysql = mysqli_connect("localhost","andresraygadas","","mascotas");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

function getTabla($tabla) {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM '.$tabla;
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        for($i=0; $i<count($fila); $i++) {
            // use of numeric index
            echo $fila[$i].' '; 
        }
        echo '<br>';
    }
    echo '<br>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
}

function getRaza($db, $razaId){
    //Specification of the SQL query
    $query='SELECT tipo FROM raza WHERE id="'.$razaId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $raza = $fila["tipo"];
    return $raza;
}

function getRegistro($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM registro WHERE id="'.$registroId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $fila;
}

function getMascotasPerdidasCards() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM registro ORDER BY created_at desc';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $cards = "";
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$cards .= '
    	<div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">'.$fila["nombre"].' ('.getRaza($db, $fila["raza_id"]).')</span>
              <p>'.$fila["ubicacion"].'</p>
              <br><br>
              <p>'.$fila["created_at"].'</p>
            </div>
            <div class="card-action">
             <a href="editar.php?id='.$fila["id"].'">'.Editar.'</a>
            </div>
          </div>
        </div>
      </div>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $cards;
}

function getMascotasPerdidasTable() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM registro';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Raza</th>
              <th>Ubicación</th>
              <th>Fecha</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["nombre"].'</td>
            <td>'.getRaza($db, $fila["raza_id"]).'</td>
            <td>'.$fila["ubicacion"].'</td>
            <td>'.$fila["created_at"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}

function getDropdownRaza($tabla, $order_by_field, $selected=-1) {
     $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM '.$tabla.' ORDER BY '.$order_by_field;
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    $select = '<div class="input-field col s12">
                <select name="'.$tabla.'_id">';
      
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        if ($selected == $fila["id"]) {
            $select .= '<option value="'.$fila["id"].'" selected>'.$fila["$order_by_field"].'</option>';
        } else {
            $select .= '<option value="'.$fila["id"].'">'.$fila["$order_by_field"].'</option>';
        }
        
    }
    $select .= '</select><label for="'.$tabla.'">'.$tabla.'</label></div>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $select;
}

function guardarRegistro($nombre, $raza_id, $ubicacion){
    $db = conectar();
    
    // insert command specification 
    $query='INSERT INTO registro (`nombre`, `raza_id` ,`ubicacion`) VALUES (?,?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $nombre, $raza_id, $ubicacion)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    desconectar($db);
}

function editarRegistro($id, $nombre, $raza_id, $ubicacion){
    $db = conectar();
    
    // insert command specification 
    $query='UPDATE registro SET nombre=?, raza_id=?, ubicacion=? WHERE  ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssss", $nombre, $raza_id, $ubicacion, $id)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
 

    
    desconectar($db);
}

function getUbicacion() {
    $db = conectar();
    $query='SELECT * FROM registro';
    $result = $db->query($query);

    $ubicacion = array();
    while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
        $ubicacion[] = $fila['ubicacion'];
    }
    mysqli_free_result($result);
    return $ubicacion;
}

?>