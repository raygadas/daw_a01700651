<?php
    session_start();
    require_once("modelo.php");
    if(isset($_POST["id"])) {
        editarRegistro($_POST["id"], $_POST["nombre"], $_POST["raza_id"], $_POST["ubicacion"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se actualizó correctamente';
    } else {
        guardarRegistro($_POST["nombre"], $_POST["raza_id"], $_POST["ubicacion"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se registró correctamente';
    }
    header("location:index.php");
?>