-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 18:38:12
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `mascotas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `raza`
--

CREATE TABLE IF NOT EXISTS `raza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `raza`
--

INSERT INTO `raza` (`id`, `tipo`) VALUES
(1, 'cocker spaniel'),
(2, 'pug'),
(3, 'beagle'),
(4, 'golden retriever'),
(5, 'pastor alemán'),
(6, 'chihuahua'),
(7, 'Chow Chow'),
(8, 'Mastín inglés');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE IF NOT EXISTS `registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `raza_id` int(11) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `raza_id` (`raza_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`id`, `nombre`, `raza_id`, `ubicacion`, `created_at`) VALUES
(1, 'Punky', 1, 'San Cristóbal de Las Casas, Chiapas', '2017-10-06 19:37:28'),
(2, 'Oc', 4, 'Jurica', '2017-10-06 19:43:05'),
(3, 'Muñe', 4, 'Ocasus', '2017-09-28 00:00:00'),
(4, 'Icker', 8, 'CDMX', '2017-10-06 00:00:00'),
(5, 'Schubert', 3, 'Querétaro Centro Sur', '0000-00-00 00:00:00'),
(6, 'Alan', 2, '4309', '2017-10-06 19:37:49'),
(7, 'Max', 7, 'Juriquilla', '2017-10-06 19:13:16'),
(8, 'Bobby', 3, 'Jurica', '2017-10-06 19:42:35'),
(9, 'Gary', 8, 'Bikini Bottom', '2017-10-06 19:43:39'),
(10, ' ', 3, ' ', '2017-10-06 19:44:29'),
(11, ' ', 6, ' ', '2017-10-06 19:44:41'),
(12, ' ', 3, ' ', '2017-10-06 19:44:51'),
(13, ' ', 3, ' ', '2017-10-06 19:45:02'),
(14, ' ', 3, ' ', '2017-10-06 19:45:16'),
(15, 'Me siento mal', 3, ':(', '2017-10-06 19:45:37');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `registro_ibfk_1` FOREIGN KEY (`raza_id`) REFERENCES `raza` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
