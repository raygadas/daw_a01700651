<?php

function conectar() {
    $servername =   'localhost';
    $username   =   'andresraygadas';
    $password   =   '';
    $nombrebd   =   'lab14';
    
    $mysql = mysqli_connect($servername,$username,$password,$nombrebd);
    $mysql->set_charset("utf8");
    return $mysql;
}

function  desconectar($con) {
    mysqli_close($con);
}

function getPublicacion($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM publicacion WHERE id="'.$registroId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $publicacion;
}

function getNoticiaCard($n){
    $db = conectar();
    
    $query='SELECT * FROM publicacion ORDER BY fecha DESC LIMIT '.($n-1).', 1';
    $registros = $db->query($query);   
    $publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH);
    
    $imagen =   $publicacion["imagen"];
    $fecha =    date('F d, Y', strtotime($publicacion["fecha"]));
    $titulo =   $publicacion["titulo"];
    $cuerpo =   $publicacion["cuerpo"];
    $id = $publicacion["id"];
    
    $card = '';
    
    
    if($n == 1){
        $card .=
        "<div class='card z-depth-0' id='noticia1'>
            <div class='card-image'>
                <img src='images/".$imagen."'>
            </div>
            <div class='card-content left-align'>
                <div class='pink white-text date-wrapper center-align'>
                    <p><small>".$fecha."</small></p>
                </div>
                <br>
                <span class='card-title'>".$titulo."</span>
                <p class='grey-text text-darken truncate-long-text' data-truncate=>".$cuerpo."</p>
            </div>
            <div class='card-action'>
                <a  onclick='articulo($id, \"editar\")'><i class='material-icons'>edit</i></a>
            </div>
        </div>
        ";
    }elseif($n <= 5){
        $card .=
        "
        <div class='row'>
            <div class='col s12'>
                <div class='card horizontal z-depth-0' id='noticia".$n."'>
                    <div class='card-image valign-wrapper noticias-small-img-container'>
                        <img src='images/".$imagen."'>
                    </div>
                    <div class='card-stacked'>
                        <div class='card-content'>
                            <p  class='pink-text'><small><i class='material-icons tiny'>date_range</i>".$fecha."</small></p>
                            <p><strong><big>".$titulo."</big></strong></p>
                            <p class='grey-text text-darken truncate-text'>".$cuerpo."</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col s12'>
                <div class='card-action'>
                    <a  onclick='articulo($id, \"editar\")'><i class='material-icons'>edit</i></a>
                </div>
            </div>
        </div>
        ";
    }
    
    desconectar($db);

    return $card;
}