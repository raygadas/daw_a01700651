function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } else {
    // Non AJAX browsers
    return(null);
  }
}


function articulo(id, instruccion){
   let JQUERY = 1;
   if (instruccion === 'editar'){
        if(JQUERY == 1) {
            $.getJSON("articulo.php", { id: id })
                .done(function( data ) {
                    alert(typeof(data));
                    var modal = document.getElementById('articulo-modal');
                    var header = modal.getElementsByTagName('h1');
                    var titulo = modal.getElementsByClassName('titulo');
                    var cuerpo = modal.getElementsByClassName('cuerpo');
                    var imagenes = modal.getElementsByClassName('imagenes');
                    var fijo = modal.getElementsByClassName('fijo');
                    
                    header[0].innerText = 'Editar artículo - con JQUERY'
                    titulo[0].value = data.titulo;
                    cuerpo[0].value = data.cuerpo;
                    imagenes[0].value = data.imagen;
                    
                    if (data.fijo === 1){
                        fijo[0].checked = true;
                    }else{
                        fijo[0].checked = false;
                    }
                    
                    $('#'+modal.id).modal('open');
                
            });
        console.log("Fin AJAX con jquery");
       } else {
           
           console.log("AJAX sin jquery");
           
           request=getRequestObject();
           if(request!=null)
           {
                var url='articulo.php?id='+id;
                request.open('GET',url,true);
                request.onreadystatechange = 
                    function() { 
                        if((request.readyState==4)){
                        // Asynchronous response has arrived
                        var data = JSON.parse(request.responseText);
                        var modal = document.getElementById('articulo-modal');
                        var header = modal.getElementsByTagName('h1');
                        var titulo = modal.getElementsByClassName('titulo');
                        var cuerpo = modal.getElementsByClassName('cuerpo');
                        var imagenes = modal.getElementsByClassName('imagenes');
                        var fijo = modal.getElementsByClassName('fijo');
                        
                        header[0].innerText = 'Editar artículo - sin JQUERY'
                        titulo[0].value = data.titulo;
                        cuerpo[0].value = data.cuerpo;
                        imagenes[0].value = data.imagen;
                        
                        if (data.fijo === 1){
                            fijo[0].checked = true;
                        }else{
                            fijo[0].checked = false;
                        }
                        
                        $('#'+modal.id).modal('open');
                        }     
                    };
            request.send(null);
            }
        }
    }else if (instruccion === 'nuevo'){
        
        var modal = document.getElementById('articulo-modal');
        var header = modal.getElementsByTagName('h1');
        var titulo = modal.getElementsByClassName('titulo');
        var cuerpo = modal.getElementsByClassName('cuerpo');
        var imagenes = modal.getElementsByClassName('imagenes');
        var fijo = modal.getElementsByClassName('fijo');
        
        header[0].innerText = 'Crear nuevo artículo';
        titulo[0].value = '';
        cuerpo[0].value = '';
        imagenes[0].value = '';
        
        fijo[0].checked = false;
        
        $('#'+modal.id).modal('open');
                
        console.log("Fin AJAX con jquery");
    } 
}

function autoCompletar() {
   let JQUERY = 0;
   var modal = document.getElementById('articulo-modal');
   var titulo = modal.getElementsByClassName('titulo');
   if(JQUERY == 1) {
        $.get("autocompletar.php", { pattern: titulo[0].value })
            .done(function( data ) {
                var ajaxResponse = document.getElementById('ajaxResponse');
                ajaxResponse.innerHTML = data;
                ajaxResponse.style.visibility = "visible";
         });
         
        console.log("Fin AJAX con jquery");
       
   } else {
       
       console.log("AJAX sin jquery");
       
       request=getRequestObject();
       if(request!=null)
       {
            var modal = document.getElementById('articulo-modal');
            var titulo = modal.getElementsByClassName('titulo');
            var url='autocompletar.php?pattern='+titulo[0].value;
            request.open('GET',url,true);
            request.onreadystatechange = 
                function() { 
                    if((request.readyState==4)){
                        // Asynchronous response has arrived
                        var ajaxResponse=document.getElementById('ajaxResponse');
                        ajaxResponse.innerHTML=request.responseText;
                        ajaxResponse.style.visibility="visible";
                    }     
                };
         request.send(null);
       }
   }

}



function selectValue() {

    console.log("selectValue");
   var list=document.getElementById("list");
   var userInput=document.getElementById("ubicacion");
   var ajaxResponse=document.getElementById('ajaxResponse');
   userInput.value=list.options[list.selectedIndex].text;
   ajaxResponse.style.visibility="hidden";
   userInput.focus();
}
