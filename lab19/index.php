<?php 
    session_start();
    require_once("modelo-noticias.php");
    include '_header.html';
?>

<main>
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Últimas Publicaciones (LAB16)</h5>
            <p>**Use AJAX para cargar el modal con los datos para editar.
            También lo use para abrir el modal en blanco y para autocompletar el campo titulo**</p>
            <div class="row" id="noticias">
                <div class="col s12 m6 l4 center-align valign-wrapper main-news">
                    <?php echo getNoticiaCard(1); ?>
                </div>

                <div class="col s12 m6 l5 other-news">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(2);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(3);?> 
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(4);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(5);?>
                        </div>
                    </div>
                </div>
            
                <div class="col s12 m12 l3 valign-wrapper feed-twitter">
                            <a class="twitter-timeline" data-height="inherit" data-lang="es" data-theme="light" data-link-color="#2B7BB9" 
                                data-chrome="nofooter" href="https://twitter.com/pcivilqro?ref_src=twsrc%5Etfw">Tweets de CEPCQ
                            </a> 
                            <script async src="//platform.twitter.com/widgets.js" 
                            charset="utf-8"></script>
                </div>
                        
                    
            </div>
    

        </div>
    </div>
    
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Preguntas (LAB16)</h5>
            <ul>
                <li><strong>Explica y elabora un diagrama sobre cómo funciona AJAX con jQuery.</strong></li>
                <li>JQUERY es una librería de JavaScript que, junto con AJAX, permite crear aplicaciones interactivas y dinámicas
                porque no es necesario recargar la página completa para mostrar nueva información; solo se recargan las partes
                necesarias. Las peticiones se inician a través de JQUERY, después el engine de AJAX realiza la petición de los datos 
                y se envia a un URL, estas peticiones se realizan en segundo plano mientras el cliente sigue trabajando. Despues, el
                servidor realiza una petición a la base de datos, la base de datos regresa la información solicitada y el servidor 
                regresa la información en un formato como puede ser JSON. Cuando se recibe una respuesta, se ejecuta una función 
                "callback" para manejar los datos y cambiar el contenido de la página.
                </li>
                
                <li><strong>¿Qué alternativas a jQuery existen?</strong></li>
                <li>Algunas alternativas son AngularJS, BackboneJS y ReactJS. También existen otras librerías para hacer peticiones
                AJAX usando el framework React, como Fetch API, Superagent, Axios y Request.
                </li> 
            </ul>
        </div>
    </div>

<?php include '_user-menu.html';?>
<?php include '_footer.html';?>
