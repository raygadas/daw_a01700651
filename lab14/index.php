<?php 
    require_once("modelo-noticias.php");
    include '_header.html';
?>

<main>
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Últimas Publicaciones (LAB14)</h5>
            <div class="row" id="noticias">
                <div class="col s12 m6 l4 center-align valign-wrapper main-news">
                    <?php echo getNoticiaCard(1); ?>
                </div>

                <div class="col s12 m6 l5 other-news">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(2);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(3);?> 
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(4);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(5);?>
                        </div>
                    </div>
                </div>
            
                <div class="col s12 m12 l3 valign-wrapper feed-twitter">
                    
                            <a class="twitter-timeline" data-height="inherit" data-lang="es" data-theme="light" data-link-color="#2B7BB9" 
                                data-chrome="nofooter" href="https://twitter.com/pcivilqro?ref_src=twsrc%5Etfw">Tweets de CEPCQ
                            </a> 
                            <script async src="//platform.twitter.com/widgets.js" 
                            charset="utf-8"></script>
                </div>
                        
                    
                </div>
    

            </div>
        </div>
    </div>
    
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Preguntas (LAB14)</h5>
            <ul>
                <li><strong>¿Qué es ODBC y para qué es útil?</strong></li>
                <li>Es un estándar para acceder a una base de datos y es muy útil porque nos sirve para acceder a los datos
                desde cualquier aplicacion independiente del DBMS de la base de datos a la cual se quiere acceder.
                </li>
                
                <li><strong>¿Qué es SQL Injection?</strong></li>
                <li>Es un ataque que se realiza a la base de datos en el cual se inserta codigo o se realizan consultar
                para obtener informacion confidencial</li>
                
                <li><strong>¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?</strong></li>
                <li>Existen diferentes formas dependiendo el DBMS, en PHP se puede usar mysql_real_escape_string para "sanar"
                    las entradas o, mas recomendable aun, usar sentencias preparadas. En general se puede: escapar los 
                    caracteres especiales utilizados en las consultas SQL, delimitar los valores de las consultas, 
                    Verificar siempre los datos que introduce el usuario, asignar mínimos privilegios al usuario que conectará
                    con la base de datos.
                </li>
            </ul>
        </div>
    </div>
<?php include '_footer.html';?>
