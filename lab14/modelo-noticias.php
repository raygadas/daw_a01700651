<?php

setlocale(LC_ALL, "ES_ES");
setlocale(LC_ALL,"es_ES");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


function conectar() {
    $servername =   'localhost';
    $username   =   'andresraygadas';
    $password   =   '';
    $nombrebd   =   'lab14';
    $con = mysqli_connect($servername,$username,$password,$nombrebd);
    
    if (!$con){
        die('Connection failed: ' . mysqli_connect_error());
    }
    
    $con->set_charset('utf8');
    
    return $con;
}

function  desconectar($con) {
    mysqli_close($con);
}


function getImagenNoticia($db, $n){
    $query='SELECT imagen FROM publicacion ORDER BY fecha LIMIT '.($n-1).', 1';
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $imagen = $fila["imagen"];
    return $imagen;
}

function getFechaNoticia($db, $n){
    $query='SELECT fecha FROM publicacion ORDER BY fecha LIMIT '.($n-1).', 1';
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $fecha = $fila["fecha"];
    return $fecha;
}

function getTituloNoticia($db, $n){
    $query='SELECT titulo FROM publicacion ORDER BY fecha LIMIT '.($n-1).', 1';
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $titulo = $fila["titulo"];
    return $titulo;
}

function getCuerpoNoticia($db, $n){
    $query='SELECT cuerpo FROM publicacion ORDER BY fecha LIMIT '.($n-1).', 1';
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $cuerpo = $fila["cuerpo"];
    return $cuerpo;
}

function getNoticiaCard($n){
    $db = conectar();
    
    $image  =   getImagenNoticia($db, $n);
    $fecha  =   date('F d, Y', strtotime(getFechaNoticia($db, $n)));;
    $titulo =   getTituloNoticia($db, $n);
    $cuerpo =   getCuerpoNoticia($db, $n);
    
    $card = '';
    
    if($n == 1){
        $card .=
        "<div class='card z-depth-0' id='ultima_noticia'>
            <div class='card-image'>
                <img src='images/".$image."'>
            </div>
            <div class='card-content left-align'>
                <div class='pink white-text date-wrapper center-align'>
                    <p><small>".$fecha."</small></p>
                </div>
                <br>
                <span class='card-title'>".$titulo."</span>
                <p class='grey-text text-darken truncate-long-text' data-truncate=>".$cuerpo."</p>
            </div>
        </div>
        ";
    }elseif($n <= 5){
        $card .=
        '
        <div class="card horizontal z-depth-0">
            <div class="card-image valign-wrapper noticias-small-img-container">
                <img src="images/'.$image.'">
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <p  class="pink-text"><small><i class="material-icons tiny">date_range</i>'.$fecha.'</small></p>
                    <p><strong><big>'.$titulo.'</big></strong></p>
                    <p class="grey-text text-darken truncate-text">'.$cuerpo.'</p>
                </div>
            </div>
        </div>
        ';
    }
    
    desconectar($db);
    
    
    return $card;
}