//Ejercicio 2
var a_text = document.getElementById('alternate_text');
a_text.ondblclick = function () {
    a_text.style.textTransform = "uppercase";
}

a_text.onmouseover = function () {
    a_text.style.color = "red";
}

a_text.onmouseout = function () {
    a_text.style.color = "#a3a3a7";
}

//Ejercicio 3
var chicha = document.getElementById('image_color_container')
var info = document.getElementById('info');
chicha.onmouseover = function () {
    info.style.display = "block";
}

chicha.onmouseout = function () {
    info.style.display = "none";
}


//Ejercicio 4
var s = 0;
var m = 0;
var h = 0;
var cir = document.getElementById('clock');
var timer = setInterval(function () {
    s++;
    m += Math.floor(s / 60);
    h += Math.floor(m / 60);

    s = s % 60;
    m = m % 60;
    h = h % 24;

    cir.innerHTML = h + 'h ' + m + 'm ' + s + 's ';
}, 1000);


//Ejercico 5
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}