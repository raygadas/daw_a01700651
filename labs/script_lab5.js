// Lab 5 ---------------------------------------------------------------
//Ejercicio 1
var pass = document.getElementById('pass');
pass.onblur = function () {
    if (pass.value.search("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")) {
        alert("La contraseña debe tener: al menos 8 caracteres, una mayuscula, una minuscula, un caracter especial y un número.");
    } else {
        document.getElementById('pass_confirm').disabled = false;
    }
}

var pass_confirm = document.getElementById('pass_confirm');
pass_confirm.onblur = function () {
    if (!(pass.value == pass_confirm.value)) {
        alert("ERROR. Las contraseñas no coincinden.");
    } else {
        document.getElementById('submit_pass').disabled = false;
    }

}

var submit = document.getElementById("submit_pass");
submit.onclick = function () {
    if (pass.value == pass_confirm.value) {
        alert("SUCCESS. Las contraseñas coinciden.");
    } else {
        alert("ERROR. Las contraseñas no coincinden.");
    }
}

//Ejercico 2
var total_price = 0;
var city_total = 0;
var psg_total = 0;
var chivas_total = 0;
var bayern_total = 0;

function update_total_price() {
    let div = document.getElementById('total_price');
    div.innerHTML = " $" + total_price;
}

var city_qty = document.getElementById('city_qty');
city_qty.onchange = function () {
    let price = 1899;
    city_total = Number(city_qty.value) * price;
    total_price = city_total + psg_total + chivas_total + bayern_total;
    update_total_price();
}

var psg_qty = document.getElementById('psg_qty');
psg_qty.onchange = function () {
    let price = 1899;
    psg_total = Number(psg_qty.value) * price;
    total_price = city_total + psg_total + chivas_total + bayern_total;
    update_total_price();
}

var chivas_qty = document.getElementById('chivas_qty');
chivas_qty.onchange = function () {
    let price = 1899;
    chivas_total = Number(chivas_qty.value) * price;
    total_price = city_total + psg_total + chivas_total + bayern_total;
    update_total_price();
}

var bayern_qty = document.getElementById('bayern_qty');
bayern_qty.onchange = function () {
    let price = 1899;
    bayern_total = Number(bayern_qty.value) * price;
    total_price = city_total + psg_total + chivas_total + bayern_total;
    update_total_price();
}

//Ejercicio3
function verificar_string(s) {
    letras = /^[ A-Za-z]+$/;
    if (s.match(letras)) {
        return 0; // Solo contiene letras
    } else {
        alert("ERROR. El campo \'Autor\' debe contener letras únicamente.")
        return 1; // Contiene otros caracteres
    }
}

function verificar_num(n) {
    n = Number(n);
    if (n) {
        return 0; // Es un número
    } else {
        alert("ERROR. El campo \'Número de páginas\' debe contener números únicamente.")
        return 1; // Contiene otros caracteres
    }
}

function libro(titulo, autor, total_pag) {
    this.titulo = titulo;
    this.autor = autor;
    this.total_pag = total_pag;
}


var libros = [];
var agregar_libro = document.getElementById('agregar_libro');
agregar_libro.onclick = function () {
    let titulo = document.getElementById('titulo').value;
    let autor = document.getElementById('autor').value;
    let total_pag = document.getElementById('total_pag').value;

    if (titulo == "" || autor == "" || total_pag == "") {
        alert("Porfavor llena todos los campos");
    } else {
        let autor_flag = verificar_string(autor);
        let total_pag_flag = verificar_num(total_pag);

        if (autor_flag == 0 && total_pag_flag == 0) {
            let nuevo_libro = new libro(titulo, autor, total_pag);
            libros.push(nuevo_libro);
            let div = document.getElementById('mis_libros');
            div.innerHTML += nuevo_libro.titulo + ". " + nuevo_libro.autor + ". " + nuevo_libro.total_pag + ".";
        }
    }
}

var borrar_libros = document.getElementById('borrar_libros');
borrar_libros.onclick = function () {
    libros = [];
    let div = document.getElementById('mis_libros');
    div.innerHTML = "";
}

// --------------------------------------------------------------- Lab 5
