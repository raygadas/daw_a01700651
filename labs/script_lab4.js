// Lab 4 ---------------------------------------------------------------
function lab1() {
    function pregunta1() {
        var num = prompt('Ingresa un número');
        var div = document.getElementById('respuesta1');
        div.innerHTML = "";

        // Crea un elemento <table> y un elemento <tbody>
        var table = document.createElement("table");
        var tbody = document.createElement("tbody");

        var row = document.createElement("tr");

        var th = document.createElement("th");
        var th_text = document.createTextNode("Número");
        th.appendChild(th_text);
        row.appendChild(th);

        var th = document.createElement("th");
        var th_text = document.createTextNode("Cuadrado");
        th.appendChild(th_text);
        row.appendChild(th);

        var th = document.createElement("th");
        var th_text = document.createTextNode("Cubo");
        th.appendChild(th_text);
        row.appendChild(th);

        tbody.appendChild(row);

        for (var i = 1; i <= num; i++) {
            row = document.createElement("tr");
            for (var j = 1; j <= 3; j++) {
                var cell = document.createElement("td");
                var cell_text = document.createTextNode(Math.pow(i, j));
                cell.appendChild(cell_text);
                row.appendChild(cell);
            }
            tbody.appendChild(row);
        }
        table.appendChild(tbody);
        div.appendChild(table);
        table.setAttribute("border", "2");
        table.setAttribute("class", "answer_table");
    }

    function pregunta2() {
        var num1 = Math.floor(Math.random() * 100);
        var num2 = Math.floor(Math.random() * 100);
        var start = window.performance.now();
        var answ = prompt("El resultado de " + num1 + " + " + num2 + " es: ");
        var end = window.performance.now();
        var time = (end - start) / 1000;

        if (num1 + num2 == answ) {
            alert("¡Correcto! Tardaste " + time.toFixed(2) + " segundos en acertar tu respuesta.");
        } else {
            alert("¡Incorrecto! La respuesta correcta es: " + (num1 + num2) + ". Tardaste " + time.toFixed(2) + " segundos en dar tu respuesta.")
        }
    }

    function pregunta3(a) {
        var neg = ceros = pos = 0;
        a = (a.innerText);
        a = a.split(',').map(Number);

        for (i = 0; i < a.length; i++) {
            if (a[i] < 0) {
                neg++;
            } else if (a[i] == 0) {
                ceros++;
            } else {
                pos++;
            }
        }
        var div = document.getElementById('respuesta3');
        var ans = "";
        ans += "El arreglo tiene: \n";
        ans += "Negativos: " + neg + "\n";
        ans += "Ceros: " + ceros + "\n";
        ans += "Positivos: " + pos + "\n";
        alert(ans);

    }

    function pregunta4(ej) {
        a = [[1, 2, 3], [4, 6, 6]];
        b = [[1, 3, 8, 4], [3, 6, 8, 2], [2, 5, 3, 7]];

        if (ej == 1) {
            arr = a;
        } else {
            arr = b;
        }

        var ans = " ";
        for (i = 0; i < arr.length; i++) {
            var sum = 0;
            for (j = 0; j < arr[i].length; j++) {
                sum += arr[i][j];
            }
            var promedio = sum / (arr[i].length);
            ans += "<br>";
            ans += "Promedio renglon " + i + ": \t" + promedio;
        }
        var div = document.getElementById('respuesta4');
        div.innerHTML = " ";
        div.innerHTML = ans;
    }

    function pregunta5(num) {
        var num = prompt('Ingresa un número: ')
        num = String(num);
        var ans = num.split("").reverse().join("");
        alert("Tu número " + num + " con orden inverso es " + ans);
    }

    function libro(titulo, autor, total_pag) {
        this.titulo = titulo;
        this.autor = autor;
        this.leido = false;
        this.total_pag = total_pag;
        this.pag_actual = 0;

        this.status = function () {
            if (this.pag_actual < this.total_pag) {
                var msg = 'No has terminado de leer ' + this.titulo + ' por ' + this.autor + '.';
                msg += '\nVas en la página ' + this.pag_actual + ' de ' + this.total_pag + '.';
            } else {
                var msg = "Ya terminaste de leer " + this.titulo + ' por ' + this.autor + '.';
            }
            return msg;
        };

        this.terminado = function () {
            this.leido = true;
        };

        this.cambiarPagActual = function (num_pag) {
            this.pag_actual = num_pag;
            if (num_pag >= this.total_pag) {
                this.terminado();
            }
        };
    }

    var libros = [];

    function pregunta6_agregar() {
        var titulo = prompt('Titulo: ');
        var autor = prompt('Autor: ');
        var total_pag = prompt('Total de páginas: ');

        var nuevo_libro = new libro(titulo, autor, total_pag);
        libros.push(nuevo_libro);

        var div = document.getElementById('respuesta6');
        div.innerHTML += nuevo_libro.titulo + "<br>";
        alert('Número de libros que tienes: ' + libros.length);
    }

    function pregunta6_consultar() {
        var num_libro = prompt('Número del libro (del 1 al ' + libros.length + '): ');
        num_libro--;
        var consulta = libros[num_libro].status();
        alert(consulta);
    }

    function pregunta6_cambiar_pag() {
        var num_libro = prompt('Número del libro (del 1 al ' + libros.length + '): ');
        var nueva_pag = prompt('Nuevo # de página actual: ')
        num_libro--;
        libros[num_libro].pag_actual = nueva_pag;
        if (libros[num_libro].pag_actual >= libros[num_libro].total_pag) {
            alert('¡Terminaste el libro!');
        } else {
            alert('Listo, nueva página: ' + nueva_pag);
        }
    }

    function pregunta6_borrar_libros() {
        var div = document.getElementById('respuesta6');
        alert('Listo, tienes ' + libros.length + 'libros');
        libros = [];
        div.innerHTML = " ";
    }


    var btn_preg1 = document.getElementById('preg1');
    btn_preg1.onclick = function () {
        pregunta1();
    };

    var btn_preg2 = document.getElementById('preg2');
    btn_preg2.onclick = function () {
        pregunta2();
    };

    var btn_preg3_1 = document.getElementById('preg3.1');
    btn_preg3_1.onclick = function () {
        var a = document.getElementById('arreglo1_ej3');
        pregunta3(a);
    };

    var btn_preg3_2 = document.getElementById('preg3.2');
    btn_preg3_2.onclick = function () {
        var a = document.getElementById('arreglo2_ej3');
        pregunta3(a);
    };

    var btn_preg4_1 = document.getElementById('preg4.1');
    btn_preg4_1.onclick = function () {
        pregunta4(1);
    };

    var btn_preg4_2 = document.getElementById('preg4.2');
    btn_preg4_2.onclick = function () {
        pregunta4(2);
    };

    var btn_preg5 = document.getElementById('preg5');
    btn_preg5.onclick = function () {
        pregunta5();
    };

    var btn_preg6_agregar = document.getElementById('preg6_agregar');
    btn_preg6_agregar.onclick = function () {
        pregunta6_agregar();
    };

    var btn_preg6_consultar = document.getElementById('preg6_consultar');
    btn_preg6_consultar.onclick = function () {
        pregunta6_consultar();
    };

    var btn_preg6_cambiar_pag = document.getElementById('preg6_cambiar_pag');
    btn_preg6_cambiar_pag.onclick = function () {
        pregunta6_cambiar_pag();
    };

    var btn_preg6_borrar_libros = document.getElementById('preg6_borrar_libros');
    btn_preg6_borrar_libros.onclick = function () {
        pregunta6_borrar_libros();
    };
}
// --------------------------------------------------------------- Lab 4