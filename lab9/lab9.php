<?php
function getAverage($a){
  $sum = 0;
  $n = count($a);
  foreach ($a as $value){
    $sum += $value;
  }
  $promedio = $sum / $n;
  return $promedio;
}

function getMedian($a){
  sort($a);
  $n = count($a);
  if ($n % 2 == 0){
    $median = ($a[$n/2]+$a[($n/2)-1])/2;
  }
  else {
    $median = $a[$n/2];
  }
  return $median;
}

function displayArray($a){
    foreach ($a as $value) {
        $array .= $value . " ";
    }
    return $array;
}

function getArrayInfo($a) {
        $list .= "<h3>Información del Arreglo</h3>";
        $list .= "<ul>";
    
        $list .= "<li>Arreglo: " . displayArray($a) . "</li>";    
        $list .= "<li>Promedio: " . getAverage($a) . "</li>";
        $list .= "<li>Mediana: " . getMedian($a) . "</li>";
        
        sort($a);
        $list .= "<li>De menor a mayor: " . displayArray($a) . "</li>";
    
        rsort($a);
        $list .= "<li>De mayor a menor: " . displayArray($a) . "</li>";
        
        $list .= "</ul>";
        
        return $list;
}

function getSqrCube($n) {
        $table = "<h3>Tabla de potencias</h3>";
        $table .= "<table>";
    
        $table .= "<thead>";
        $table .= "<tr>";
        $table .= "<th>Numero</th><th>Cuadrado</th><th>Cubo</th>";
        $table .= "</tr>";
        $table .= "</thead>";
    
        $table .= "<tbody>";    
        for ($i = 1; $i <= $n; $i++) {
            $table .= "<tr>";
            $table .= "<td>$i</td><td>".$i*$i."</td><td>".$i*$i*$i."</td>";
            $table .= "</tr>";
        }
        $table .= "</tbody>";
    
        $table .= "</table>";
        
        return $table;
}

function getFactors($n) {
    $list .= "<h3>List de Factores de ". $n . "</h3>";
    $list .= "<ul>";
    $list .= "<li>";
    $factors = array();
    
    for ($i = 1; $i <= $n; $i++){
        if ($n % $i == 0 ){
            array_push($factors, $i);
        }
    }
    
    $list .= displayArray($factors);
    $list .= "</li>";
    $list .= "</ul>";
    return $list;
}

function getAnswers() {
    $answers .= "<h3>Preguntas</h3>";
    $answers .= "<h4>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h4>";
    $answers .= "<p>Despliega grandes cantidaddes de información acerca de el estado actual de PHP. Por ejemplo, opciones de compliación y extensiones, versión, información del servidor y del ambiente de PHP, entre otros. A mi una de las cosas que mas me llamo la atencion fue que despliega la ruta donde esta el archivo guardado en mi computadora, que en este caso es el servidor. Me intriga que en Default Timezone aparezca Europe/Berlin, no se ni donde esta esa configuracion o como podria cambiarla. Tambien me parecio interesante que vengan los creditos para los autores del lenguaje, los modulos y la documentacion, creo que es bueno ver que se reconoce el trabajo de estas personas.</p>";
    
    $answers .= "<h4>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h4>";
    $answers .= "<p>Básicamente, XAMPP, WAMPP y MAMP son distribuciones de APACHE para desarrollo y no tienen los niveles de protección que debe tener un servidor que está en producción dando hosting a aplicaciones o páginas web. Debemos desactivar el desplegado de errores para que los usuarios que accedan a la aplicación web, no vean ni sepan cuáles son estos errores y así añadir protección a tu sistema. Sin embargo es bueno llevar un log de los errores, por ejemplo en un archivo de nombre error.log, para poder arreglar estos errores. La principal diferencia entre un servidor de desarrollo y uno en producción son cuestiones de seguridad. El que esta en un ambiente para desarrollo permite acceso no restringido y control de un usuario o grupo de usuarios. El que esta en un ambiente de producción, restringe el acceso a usuarios autorizados y limita el control del sistema. Por ejemplo, en un ambiente de desarrollo cualquiera puede apagar el servidor, en un ambiente de producción solo el administrador podría tener estos privilegios.  </p>";
    
    $answers .= "<h4>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h4>";
    $answers .= "<p>El código PHP siempre se ejecuta del lado del servidor, de hecho es un lenguaje de servidor, y el código o archivo que se despliega del lado del cliente es en html. Con PHP podemos producir código en HTML, como en este laboratorio.</p>";
    $answers .= "<h4>Referencias:</h4>";
    $answers .= "<ul>
    <li>https://documentation.progress.com/output/ua/OpenEdge_latest/index.html#page/pasoe-admin/development-servers-compared-to-production-serve.html</li>
    <li>https://www.codementor.io/php/tutorial/how-to-setup-php-development-production-server</li>
    </ul>";
    
    return $answers;
}

$array1 = array(2, 4, 8, 9, 12, 10);
$array2 = array(1, 4, 5, 10, 15);

echo "Promedio del arreglo 1: " . getAverage($array1) . "<br />";
echo "Mediana del arreglo 1: " . getMedian($array1). "<br />";

echo "Promedio del arreglo 2: " . getAverage($array2). "<br />";
echo "Mediana del arreglo 2: " . getMedian($array2). "<br />";

$list = getArrayInfo($array1);
echo $list;

$list = getArrayInfo($array2);
echo $list;

$table = getSqrCube(5);
echo $table;

$factors = getFactors(45);
echo $factors;

$factors = getFactors(72);
echo $factors;

$answers = getAnswers();
echo $answers;

phpinfo();
?>
