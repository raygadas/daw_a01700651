<?php
    if(!empty($_POST["nombre"]) && !empty($_POST["apellido"]) && !empty($_POST["email"]) && !empty($_POST["mensaje"])) {
        // limpiar todas las entradas de espacios en blanco al principio/final, backslashes y 
        // usando htmlspecialchars para evitar XSS
        $nombre = clean_input($_POST["nombre"]);
        $apellido = clean_input($_POST["apellido"]);
        $email = clean_input($_POST["email"]);
        $mensaje = clean_input($_POST["mensaje"]);
        
        // Variable para ver si hubo errores, inicializacion de errores en vacio
        $flag = 0;
        $nombreErr = "";
        $apellidoErr = "";
        $emailErr = "";
        $mensajeErr = "";
        
        // checar que nombe y apellido contengan solo letras y espacios en blanco
        if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
            $nombreErr = "El nombre solo acepta letras y espacios en blanco. ";
            $flag = 1;
        }
        if (!preg_match("/^[a-zA-Z ]*$/",$apellido)) {
            $apellidoErr = "El apellido solo acepta letras y espacios en blanco. "; 
            $flag = 1;
        }
        
        // checar que el mail sea valido
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "El correo electrónico no es válido. "; 
            $flag = 1;
        }
        
        
        // Checar longitud de cada campo
        if(strlen($nombre) > 50){
            $nombreErr .= "El nombre no puede tener más de 50 caracteres";
        }
        
        if(strlen($apellido) > 50){
            $apellidoErr .= "El apellido no puede tener más de 50 caracteres";
        }
        
        if(strlen($mail) > 100){
            $mailErr .= "El mail no puede tener más de 100 caracteres";
        }
        
        if(strlen($mensaje) > 50){
            $mensajeErr .= "El mensaje no puede tener más de 1500 caracteres";
        }
        
        
        if ($flag == 0) {
            include('_header.html');
            include('_peticionExitosa.html');
            include('_footer.html');
        }
        
        elseif ($flag == 1) {
            include("_header.html");
            include("_viewTramiteAforos.html");
            echo "
            <div class='container red-text'>
            <h5>
                ERRORES
            </h5>
            <p>
                Nombre: " . $nombreErr . "
            </p>
            <p>
                Apellido: " . $apellidoErr . "
            </p>
            <p>
                Email: " . $emailErr . "
            </p>
            <p>
                Mensaje: " . $mensajeErr . "
            </p>
            </div>
            ";
            include("_preguntas.html");
            include("_footer.html");
        }
        
    } else {
        header("HTTP/1.0 404 Not Found");
    }
    
    
    
    function clean_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
?>