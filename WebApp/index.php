<?php include '_header.html';?>

<main>
    
    <div class="slider-container">
        <div id="main-slider">
                <div class="slider">
                    <ul class="slides">
                        <li>
                            <img src="images/banner1.jpg">
                            <!-- random image -->
                            <div class="caption left-align">
                                <h3>Protección Civil somos todos</h3>
                                <h5 class="light grey-text text-lighten-3">Lorem ipsum dolor sit amet viverra venenatis.</h5>
                            </div>
                        </li>
                    </ul>
                </div>
        </div>
    </div>
    
    <div class="section dark-blue">
        <div class="row">
            
        </div>
    </div>
    

    <!--Seccion de "Qué Atendemos" con 3 fenomenos-->
    <div class="section grey lighten-4">
        <div class="container">
            <h5 class="center-align uppercase dark-blue-hover-text">Qué es protección civil</h5>
            <div class="row valign-wrapper">
                <div class="col s12 m12 l12">
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo enim genera quae erant, 
                            fecit tria. Quid de Platone aut de Democrito loquar? Minime vero istorum quidem, inquit. 
                            Teneo, inquit, finem illi videri nihil dolere. Nunc ita separantur, ut disiuncta sint, 
                            quo nihil potest esse perversius. Quod non faceret, si in voluptate summum bonum poneret. 
                        </p>
                </div>
            </div>
        
            <div class="row valign-wrapper">
                <div class="col s12 m6 l6">
                    <h6 class="center-align uppercase dark-blue-hover-text" >Atendemos</h6>
                    <div class="row">
                        <div class="col s12 m4 l4">
                            <div class="card-panel hoverable">
                                <span class="white-text">ICONO Desastres Naturales</span>
                                <span class="desastres-nat"><img src="images/terremoto.png" class="responsive-img" alt=""></span>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card-panel hoverable">
                                <span class="white-text">ICONO Desastres Antrópicos</span>
                                <span class="desastres-nat"><img src="images/terremoto.png" class="responsive-img" alt=""></span>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card-panel hoverable">
                                <span class="white-text">ICONO Desastres Astronómicos</span>
                                <span class="desastres-nat"><img src="images/terremoto.png" class="responsive-img" alt=""></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l6">
                    <h6 class="center-align uppercase dark-blue-hover-text" >Tramitamos</h6>
                    <div class="row">
                        <div class="col s12 m4 l4">
                            <div class="card-panel hoverable">
                                <span class="white-text">ICONO Desastres Naturales</span>
                                <span class="desastres-nat"><img src="images/terremoto.png" class="responsive-img" alt=""></span>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card-panel hoverable">
                                <span class="white-text">ICONO Desastres Antrópicos</span>
                                <span class="desastres-nat"><img src="images/terremoto.png" class="responsive-img" alt=""></span>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card-panel hoverable">
                                <span class="white-text">ICONO Desastres Astronómicos</span>
                                <span class="desastres-nat"><img src="images/terremoto.png" class="responsive-img" alt=""></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!--Seccion Últimas Noticias-->
<div class="section">
    
    <h5 class="center-align uppercase pink-text">Últimas Publicaciones</h5>
    <div class="row"  id="noticias">

            <div class="col s12 m4 l4 center-align valign-wrapper">
                <div class="card z-depth-0" id="ultima_noticia">
                    <div class="card-image">
                        <img src="http://queretaro.swq.mx/wp-content/uploads/2017/08/turismo-el-acueducto-de-queretaro-02.jpg">
                        
                    </div>
                    <div class="card-content left-align">
                        <div class="pink white-text date-wrapper center-align"><p><small>Oct 29, 2017</small></p></div>
                        <br>
                        <span class="card-title">Card Title</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo enim genera quae erant, 
                            fecit tria. Quid de Platone aut de Democrito loquar? Minime vero istorum quidem, inquit. 
                            Teneo, inquit, finem illi videri nihil dolere. Nunc ita separantur, ut disiuncta sint, 
                            quo nihil potest esse perversius. Quod non faceret, si in voluptate summum bonum poneret. 
                            Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Quod non faceret,
                            si in voluptate summum bonum poneret. Si in voluptate summum bonum poneret.</p>
                    </div>
                </div>
            </div>
            
            <div class="col s12 m5 l5 other-news">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card horizontal z-depth-0">
                            <div class="card-image valign-wrapper">
                                <img src="https://lorempixel.com/100/190/nature/6">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p><small><i class="material-icons tiny">date_range</i> Oct 29, 2017</small></p>
                                    <p><stron><big>Titulo de la noticia</big></stron></p>
                                    <p>I am a very simple card. I am good at containing small bits of information.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="divider"></div>
                
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card horizontal z-depth-0">
                            <div class="card-image valign-wrapper">
                                <img src="https://lorempixel.com/100/190/nature/6">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p><small><i class="material-icons tiny">date_range</i> Oct 29, 2017</small></p>
                                    <p><big>Titulo de la noticia</big></p>
                                    <p>I am a very simple card. I am good at containing small bits of information.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="divider"></div>
                
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card horizontal z-depth-0">
                            <div class="card-image valign-wrapper">
                                <img src="https://lorempixel.com/100/190/nature/6">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p><small><i class="material-icons tiny">date_range</i> Oct 29, 2017</small></p>
                                    <p><big>Titulo de la noticia</big></p>
                                    <p>I am a very simple card. I am good at containing small bits of information.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="divider"></div>
                
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card horizontal z-depth-0">
                            <div class="card-image valign-wrapper">
                                <img src="https://lorempixel.com/100/190/nature/6">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p><small><i class="material-icons tiny">date_range</i> Oct 29, 2017</small></p>
                                    <p><big>Titulo de la noticia</big></p>
                                    <p>I am a very simple card. I am good at containing small bits of information.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col s12 m3 l3 blue valign-wrapper"></div>
        
    </div>
    
</div>
    

    
    <!--Seccion Próximos Eventos-->
    <div class="section grey lighten-4">
    <div class="container prox_eventos5">
        <h5 class="center-align uppercase green-text">Próximos Talleres</h5>
        <div class="divider"></div>
        <div class="row">
            <div class="col s4 m4 l4">
                <div class="card grey lighten-3">
                    <div class="center-align">
                        <p>Viernes</p>
                        <h5>FEB</h5>
                        <h1>19</h1>
                        <h5>2017</h5>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4"><p><small></small><i class="material-icons right">more_vert</i></p></span>
                        <br>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                        <ul>
                            <li>Evento 1</li>
                            <li>Evento 2</li>
                            <li>Evento 3</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col s4 m4 l4">
                <div class="card grey lighten-3">
                    <div class="center-align">
                        <p>Viernes</p>
                        <h5>FEB</h5>
                        <h1>23</h1>
                        <h5>2017</h5>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4"><p><small></small><i class="material-icons right">more_vert</i></p></span>
                        <br>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                        <ul>
                            <li>Evento 1</li>
                            <li>Evento 2</li>
                            <li>Evento 3</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col s4 m4 l4">
                <div class="card grey lighten-3">
                    <div class="center-align">
                        <p>Viernes</p>
                        <h5>FEB</h5>
                        <h1>26</h1>
                        <h5>2017</h5>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4"><p><small></small><i class="material-icons right">more_vert</i></p></span>
                        <br>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                        <ul>
                            <li>Evento 1</li>
                            <li>Evento 2</li>
                            <li>Evento 3</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</main>

<?php include '_footer.html';?>
