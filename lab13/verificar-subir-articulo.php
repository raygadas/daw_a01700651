<?php session_start();?>

<?php include '_header.html';?>

<?php

    $titulo     = htmlspecialchars($_POST['titulo']);
    $cuerpo     = htmlspecialchars($_POST['cuerpo']);
    $fotos      = htmlspecialchars($_POST['fotos']);
    $archivos   = htmlspecialchars($_POST['archivos']);
    $video      = htmlspecialchars($_POST['video']);
    $fijar      = htmlspecialchars($_POST['fijar']);
    $fecha      = htmlspecialchars($_POST['fecha']);
    $hora       = htmlspecialchars($_POST['hora']);
   

if(isset($titulo) && isset($cuerpo) && isset($fotos) && isset($archivos) && isset($video)
    && isset($fijar) && isset($fecha) && isset($hora)) {
        
    $_SESSION['titulo']     = $titulo;
    $_SESSION['cuerpo']     = $cuerpo;
    $_SESSION['fotos']      = $fotos;
    $_SESSION['archivos']   = $archivos;
    $_SESSION['video']      = $video;
    $_SESSION['fijar']      = $fijar;
    $_SESSION['fecha']      = $fecha;
    $_SESSION['hora']       = $hora;
    
    $_SESSION['fijoFechaHoraFin']   = $fecha . " " . $hora;
    $format = "d F, Y H:i";
    
    $_SESSION['fijoFechaHoraFin'] = DateTime::createFromFormat($format, $_SESSION['fijoFechaHoraFin']);

    include '_subir-archivo-exitoso.html';
  
}else {
    include '_subir-archivo-fallido.html';
}

session_unset(); 
session_destroy();
include '_footer.html'; 

?>

<div class="container left-align">
    <p><strong>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</strong></p>
    <p>session_unset libera las variables de la sesion, pero la sesion sigue activa y aun existe. Con session_destroy
    destruyes la inforamcion pero no liberas las variables, por lo cual para usar esas variables basta con volver a 
    llamar a session_start(). Usamos session_unser() para liberar las variables y luego session_destroy() para destruir
    la sesions</p>
    
    <p><strong>¿Cuál es la diferencia entre una variable de sesión y una cookie?</strong></p>
    <p>Las variables de sesión se guardan en el servidor mientras que las cookies existen del lado del cliente.</p>
    
    <p><strong>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de 
    archivos cuando sube una foto con el mismo nombre?</strong></p>
    <p>Añadiendo un identificador único, concatenar valores como la fecha o el nombre de usuario, usar la llave de la tabla
    entre otras cosas.</p>
    
    <p><strong>¿Qué es CSRF y cómo puede prevenirse?</strong>EL CSRF es un ataque que basicamente hace que un usuario ejecute
    no autorizados sin que lo pretenda. Por ejemplo, con este ataque se puede hacer que cambie el mail de una cuenta sin que
    el usuario quiera cambiarlo, se solicita una contraseña nueva y con ello se roba la cuenta. Para prevenirlo hay que asociar
    un único al usuario para verificarlo en cada acción</p>
    <p></p>
</div>

