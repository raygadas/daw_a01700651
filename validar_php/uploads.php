<?php
 
if (isset($_FILES['imagenes']) && !empty($_FILES['imagenes'])) {
    $no_files = count($_FILES["imagenes"]['name']);
    for ($i = 0; $i < $no_files; $i++) {
        if ($_FILES["imagenes"]["error"][$i] > 0) {
            echo "Error: " . $_FILES["imagenes"]["error"][$i] . "<br>";
        } else {
            if (file_exists('uploads/' . $_FILES["imagenes"]["name"][$i])) {
                echo 'File already exists : uploads/' . $_FILES["imagenes"]["name"][$i];
            } else {
                move_uploaded_file($_FILES["imagenes"]["tmp_name"][$i], 'uploads/' . $_FILES["imagenes"]["name"][$i]);
                echo 'File successfully uploaded : uploads/' . $_FILES["imagenes"]["name"][$i] . ' ';
            }
        }
    }
} else {
    echo 'Please choose at least one file';
}