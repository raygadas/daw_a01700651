$(document).ready(function(e) {
    $('#form-articulo').submit(function(event) {
        event.preventDefault();  
        var form_data = new FormData();
        var ins = document.getElementById('imagenes').files.length;
        for (var x = 0; x < ins; x++) {
            form_data.append("imagenes[]", document.getElementById('imagenes').files[x]);
            console.log(x);
        }
        $.ajax({
            url: 'form_validation.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(response) {
                console.log(response);
                response = JSON.parse(response);
                console.log(response);
                $('#msg').html(response); // display success response from the PHP script
            },
            error: function(response) {
                $('#msg').html(response); // display error response from the PHP script
            }
        });
    });
    /*
    $('#form-articulo').submit(function(event) {
        event.preventDefault();  
        var form_data = new FormData();
        var ins = document.getElementById('multiFiles').files.length;
        for (var x = 0; x < ins; x++) {
            form_data.append("files[]", document.getElementById('multiFiles').files[x]);
        }
        $.ajax({
            url: 'uploads.php', // point to server-side PHP script 
            dataType: 'text', // what to expect back from the PHP script
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(response) {
                $('#msg').html(response); // display success response from the PHP script
            },
            error: function(response) {
                $('#msg').html(response); // display error response from the PHP script
            }
        });
    });
    */
});
