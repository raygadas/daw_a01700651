<?php

if(isset($_FILES['imagenes']) && !empty($_FILES['imagenes']))
{
    $response['all_uploaded'] = true;
    foreach($_FILES['imagenes']['name'] as $key=>$name){
        // Rutas para guardar
        $target_dir     = "uploads/";
        $target_file    = clean_input($target_dir . $_FILES['imagenes']['name'][$key]);
        $source_file    = clean_input($_FILES['imagenes']['tmp_name'][$key]);
        $filename       = clean_input($_FILES['imagenes']['name'][$key]);
        
        // Propiedades de las fotos
        $size           = $_FILES['imagenes']['size'][$key];
        $type           = clean_input(pathinfo($target_file,PATHINFO_EXTENSION));
        
        $uploadOk = false;
        
        // Checar que el archivo realmente sea una imagen
        if(! @is_array(getimagesize($_FILES["imagenes"]["tmp_name"][$key]) || getimagesize($_FILES["imagenes"]["tmp_name"][$key]))){
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = true;
            
            // Checar que el tamaño sea valido
            if ($size > 5000000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = false;
                $errores['tamaño'] = 'Execede el tamaño máximo';
            }
            // Checar que tenga un formato valido
            if($type != "jpg" && $type != "png" && $type != "jpeg"
            && $type != "gif" ) {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = false;
                $errores['formato'] = 'Los formatos válidos son jpg, jpeg, png y gif';
            }
        }
        // El archivo no es una imagen
        else {
            $uploadOk = false;
            $errores['tipo'] = 'Imposible subir este archivo como imagen.';
        }
        
        
        $response['imagenes'][$key]['nombre'] = $name;
        
        // Intentar subir la imagen
        if ($uploadOk == false) {
            $response['all_uploaded'] = false;
            $response['imagenes'][$key]['upload'] = false;
            $response['imagenes'][$key]['mensaje'] = $errores;
        } else {
            if (move_uploaded_file($_FILES["imagenes"]["tmp_name"][$key], 'uploads/' . $_FILES["imagenes"]["name"][$key])) {
                $response['imagenes'][$key]['upload'] = true;
                $response['imagenes'][$key]['mensaje'] = "El archivo " . $name . " es valido para subirse como imágen.";
            } else {
                $response['all_uploaded'] = false;
                $response['imagenes'][$key]['upload'] = false;
                $response['imagenes'][$key]['mensaje'] = 'El archivo ' . $name . ' no puede subirse en este momento.';
            }
        }
    }
    
    echo json_encode($response);
}
else{
    echo 'empty';
}

function clean_input($s){
    $s = preg_replace('/\s+/', '', $s);
    $s = strtolower($s);
    $s = htmlspecialchars($s);
    return $s;
}

?>