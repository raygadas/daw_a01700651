<?php 
    session_start();
    require_once("modelo-noticias.php");
    include '_header.html';
?>

<main>
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Últimas Publicaciones (LAB16)</h5>
            <p>**Has click en el boton rojo y luego en crear artículo.**</p>
            <div class="row" id="noticias">
                <div class="col s12 m6 l4 center-align valign-wrapper main-news">
                    <?php echo getNoticiaCard(1); ?>
                </div>

                <div class="col s12 m6 l5 other-news">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(2);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(3);?> 
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(4);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(5);?>
                        </div>
                    </div>
                </div>
            
                <div class="col s12 m12 l3 valign-wrapper feed-twitter">
                    
                            <a class="twitter-timeline" data-height="inherit" data-lang="es" data-theme="light" data-link-color="#2B7BB9" 
                                data-chrome="nofooter" href="https://twitter.com/pcivilqro?ref_src=twsrc%5Etfw">Tweets de CEPCQ
                            </a> 
                            <script async src="//platform.twitter.com/widgets.js" 
                            charset="utf-8"></script>
                </div>
                        
                    
            </div>
    

        </div>
    </div>
    
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Preguntas (LAB16)</h5>
            <ul>
                <li><strong>¿Por qué es una buena práctica separar el modelo del controlador?</strong></li>
                <li>Hace mas escalable el proyecto y entendible, pues tiene más organización. Además, es importante porque
                separa fragmentos de código que tienen diferentes funcionalidades, en este caso el modelo es el
                </li>
                
                <li><strong>¿Qué es SQL injection y cómo se puede prevenir?</strong></li>
                <li>Es un ataque que se realiza a la base de datos en el cual se inserta codigo o se realizan consultar
                para obtener informacion confidencial.Existen diferentes formas dependiendo el DBMS, en PHP se puede usar mysql_real_escape_string para escapar
                    las entradas o, mas recomendable aun, usar sentencias preparadas. En general se puede: escapar los 
                    caracteres especiales utilizados en las consultas SQL, delimitar los valores de las consultas, 
                    Verificar siempre los datos que introduce el usuario, asignar mínimos privilegios al usuario que conectará
                    con la base de datos.
                </li>
            </ul>
        </div>
    </div>

<?php include '_user-menu.html';?>
<?php include '_footer.html';?>
