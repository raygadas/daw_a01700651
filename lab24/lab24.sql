-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-11-2017 a las 22:41:42
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `lab24`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`andresraygadas`@`localhost` PROCEDURE `crearArticulo`(IN `titulo` VARCHAR(100), IN `cuerpo` TEXT)
BEGIN
INSERT INTO publicacion (`titulo`, `cuerpo`) VALUES (titulo,cuerpo);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE IF NOT EXISTS `publicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `titulo` varchar(100) COLLATE utf8_bin NOT NULL,
  `cuerpo` text COLLATE utf8_bin NOT NULL,
  `imagen` varchar(500) COLLATE utf8_bin NOT NULL DEFAULT 'plaza-de-armas.jpg',
  `fijo` tinyint(1) DEFAULT '0',
  `fijoFechaHoraFin` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=43 ;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`id`, `fecha`, `titulo`, `cuerpo`, `imagen`, `fijo`, `fijoFechaHoraFin`) VALUES
(5, '2017-11-12 22:39:15', 'Notica 1', 'Noticia 1 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beatus sibi videtur esse moriens. Ut aliquid scire se gaudeant? Contemnit enim disserendi elegantiam, confuse loquitur. Quippe: habes enim a rhetoribus;\r\n\r\nParia sunt igitur. Quid dubitas igitur mutare principia naturae?\r\n\r\nCur post Tarentum ad Archytam? Magna laus. Erat enim res aperta. Duo Reges: constructio interrete. Gerendus est mos, modo recte sentiat.\r\n\r\nOmnia contraria, quos etiam insanos esse vultis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Ita prorsus, inquam; Sed ille, ut dixi, vitiose. Compensabatur, inquit, cum summis doloribus laetitia. Egone quaeris, inquit, quid sentiam?\r\n\r\nComprehensum, quod cognitum non habet? Nemo igitur esse beatus potest. Tubulo putas dicere? Tibi hoc incredibile, quod beatissimum. Istic sum, inquit. Zenonis est, inquam, hoc Stoici.', 'puente-dios.jpg', 0, '0000-00-00 00:00:00'),
(7, '2017-11-12 22:38:38', 'Noticia 3', 'Noticia 3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beatus sibi videtur esse moriens. Ut aliquid scire se gaudeant? Contemnit enim disserendi elegantiam, confuse loquitur. Quippe: habes enim a rhetoribus;\r\n\r\nParia sunt igitur. Quid dubitas igitur mutare principia naturae?\r\n\r\nCur post Tarentum ad Archytam? Magna laus. Erat enim res aperta. Duo Reges: constructio interrete. Gerendus est mos, modo recte sentiat.\r\n\r\nOmnia contraria, quos etiam insanos esse vultis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Ita prorsus, inquam; Sed ille, ut dixi, vitiose. Compensabatur, inquit, cum summis doloribus laetitia. Egone quaeris, inquit, quid sentiam?\r\n\r\nComprehensum, quod cognitum non habet? Nemo igitur esse beatus potest. Tubulo putas dicere? Tibi hoc incredibile, quod beatissimum. Istic sum, inquit. Zenonis est, inquam, hoc Stoici.', 'arcos.jpg', 0, '0000-00-00 00:00:00'),
(40, '2017-11-12 22:39:00', 'Noticia 2', 'Noticia 2 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beatus sibi videtur esse moriens. Ut aliquid scire se gaudeant? Contemnit enim disserendi elegantiam, confuse loquitur. Quippe: habes enim a rhetoribus; Paria sunt igitur. Quid dubitas igitur mutare principia naturae? Cur post Tarentum ad Archytam? Magna laus. Erat enim res aperta. Duo Reges: constructio interrete. Gerendus est mos, modo recte sentiat. Omnia contraria, quos etiam insanos esse vultis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Ita prorsus, inquam; Sed', 'plaza-de-armas.jpg', 0, NULL),
(41, '2017-11-12 22:37:56', 'Noticia 4', 'Noticia 4 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beatus sibi videtur esse moriens. Ut aliquid scire se gaudeant? Contemnit enim disserendi elegantiam, confuse loquitur. Quippe: habes enim a rhetoribus;\r\n\r\nParia sunt igitur. Quid dubitas igitur mutare principia naturae?\r\n\r\nCur post Tarentum ad Archytam? Magna laus. Erat enim res aperta. Duo Reges: constructio interrete. Gerendus est mos, modo recte sentiat.\r\n\r\nOmnia contraria, quos etiam insanos esse vultis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Ita prorsus, inquam; Sed ille, ut dixi, vitiose. Compensabatur, inquit, cum summis doloribus laetitia. Egone quaeris, inquit, quid sentiam?\r\n\r\nComprehensum, quod cognitum non habet? Nemo igitur esse beatus potest. Tubulo putas dicere? Tibi hoc incredibile, quod beatissimum. Istic sum, inquit. Zenonis est, inquam, hoc Stoici.', 'puente-dios.jpg', 0, '0000-00-00 00:00:00'),
(42, '2017-11-12 22:37:47', 'Noticia 5', 'Noticia 5 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beatus sibi videtur esse moriens. Ut aliquid scire se gaudeant? Contemnit enim disserendi elegantiam, confuse loquitur. Quippe: habes enim a rhetoribus;\r\n\r\nParia sunt igitur. Quid dubitas igitur mutare principia naturae?\r\n\r\nCur post Tarentum ad Archytam? Magna laus. Erat enim res aperta. Duo Reges: constructio interrete. Gerendus est mos, modo recte sentiat.\r\n\r\nOmnia contraria, quos etiam insanos esse vultis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Ita prorsus, inquam; Sed ille, ut dixi, vitiose. Compensabatur, inquit, cum summis doloribus laetitia. Egone quaeris, inquit, quid sentiam?\r\n\r\nComprehensum, quod cognitum non habet? Nemo igitur esse beatus potest. Tubulo putas dicere? Tibi hoc incredibile, quod beatissimum. Istic sum, inquit. Zenonis est, inquam, hoc Stoici.', 'puente-dios.jpg', 0, '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
