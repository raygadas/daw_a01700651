<?php

function conectar() {
    $servername =   'localhost';
    $username   =   'andresraygadas';
    $password   =   '';
    $nombrebd   =   'lab24';
    
    $mysql = mysqli_connect($servername,$username,$password,$nombrebd);
    $mysql->set_charset("utf8");
    return $mysql;
}

function  desconectar($con) {
    mysqli_close($con);
}

function getRegistro($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM publicacion WHERE id="'.$registroId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $publicacion;
}

function getNoticiaCard($n){
    $db = conectar();
    
    $query='SELECT * FROM publicacion ORDER BY fecha DESC LIMIT '.($n-1).', 1';
    $registros = $db->query($query);   
    $publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH);
    
    $imagen =   $publicacion["imagen"];
    $fecha =    date('F d, Y', strtotime($publicacion["fecha"]));
    $titulo =   $publicacion["titulo"];
    $cuerpo =   $publicacion["cuerpo"];
    $card = '';
    
    $card .=
    '
     <div class="row">
        <div class="col s12 m12 l12">
          <div class="card">
            <div class="card-image">
              <img src="images/'.$imagen.'">
              <span class="card-title">'.$titulo.'</span>
            </div>
            <div class="card-content">
              <p>'.$cuerpo.'</p>
            </div>
            <div class="card-action">
              <p>'.$fecha.'<p>
            </div>
          </div>
        </div>
    </div>
    ';

    
    desconectar($db);

    return $card;
}