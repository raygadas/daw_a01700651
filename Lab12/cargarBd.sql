BULK INSERT a1700651.a1700651.[Materiales]
  FROM 'e:\wwroot\a1700651\materiales.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = ' '
  )

BULK INSERT a1700651.a1700651.[Proyectos]
  FROM 'e:\wwroot\a1700651\Proyectos.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = ' '
  )

BULK INSERT a1700651.a1700651.[Proveedores]
  FROM 'e:\wwroot\a1700651\Proveedores.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = ' '
  )

SET DATEFORMAT dmy -- especificar formato de la fecha

BULK INSERT a1700651.a1700651.[Entregan]
  FROM 'e:\wwroot\a1700651\Entregan.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = ' '
  )