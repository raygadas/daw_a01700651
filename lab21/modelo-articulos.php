<?php
function conectar() {
    $servername =   'localhost';
    $username   =   'andresraygadas';
    $password   =   '';
    $nombrebd   =   'lab14';
    $con = mysqli_connect($servername,$username,$password,$nombrebd);
    
    if (!$con){
        die('Connection failed: ' . mysqli_connect_error());
    }
    
    $con->set_charset('utf8');
    
    return $con;
}

function  desconectar($con) {
    mysqli_close($con);
}

function guardarArticulo($titulo, $cuerpo, $fotos, $archivos, $video, $fijo, $fechafijo, $horafijo){
    $db = conectar();
        
    $fijoFechaHoraFin = $fechafijo . ' ' . $horafijo;
    
    $stored_procedure = true;
    
    if($stored_procedure == true){
        $query='CALL `crearArticulo`(?,?);';
    }
    else{
        $query='INSERT INTO publicacion (`titulo`, `cuerpo`)';
    }

    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ss", $titulo, $cuerpo)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    desconectar($db);
}

function editarArticulo($id, $titulo, $cuerpo, $fotos, $archivos, $video, $fijo, $fechafijo, $horafijo){
    $db = conectar();
    
    // insert command specification 
    $query='UPDATE publicacion SET titulo=?, cuerpo=? WHERE id=?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $titulo, $cuerpo, $id)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
 

    
    desconectar($db);
}

?>