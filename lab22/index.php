<?php 
    session_start();
    require_once("modelo-noticias.php");
    include '_header.html';
?>

<main>
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Lab 22</h5>
            <p>**Use twitter**</p>
            <div class="row" id="noticias">
                <div class="col s12 m6 l4 center-align valign-wrapper main-news">
                    <?php echo getNoticiaCard(1); ?>
                </div>

                <div class="col s12 m6 l5 other-news">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(2);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(3);?> 
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(4);?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php echo getNoticiaCard(5);?>
                        </div>
                    </div>
                </div>
            
                <div class="col s12 m12 l3 valign-wrapper feed-twitter">
                            <a class="twitter-timeline" data-height="inherit" data-lang="es" data-theme="light" data-link-color="#2B7BB9" 
                                data-chrome="nofooter" href="https://twitter.com/pcivilqro?ref_src=twsrc%5Etfw">Tweets de CEPCQ
                            </a> 
                            <script async src="//platform.twitter.com/widgets.js" 
                            charset="utf-8"></script>
                </div>
                        
                    
            </div>
    

        </div>
    </div>
    
    <div class="section">
        <div class="container">
            <h5 class="center-align uppercase pink-text">Preguntas (LAB16)</h5>
            <ul>
                <li><strong>
                    ¿Qué ventajas y desventajas tiene la integración de tus aplicaciones web 
                    con servicios web desarrollados por terceros?
                </strong></li>
                <li>
                    Algunas ventajas son: agiliza el desarrollo porque usamos componentes complejos que alguien mas ya desarrolló,
                    usamos componentes que ya están probados en muchas situaciones y podemos estar tranquilos de que funcionan
                    correctamente, constantmente las empresas mejoran sus APIs y no tenemos que preocuparnos nosotros por darle
                    mantenimiento, como pertenecen a grandes empresas podemos estar tranquilos de que el API será mejorado por
                    la empresa (ej: Google, Microsoft, Twitter, Facebook, etc).
                </li>
            </ul>
        </div>
    </div>

<?php include '_user-menu.html';?>
<?php include '_footer.html';?>
