<?php
    session_start();
    require_once("modelo-articulos.php");
    if(isset($_POST["id"])) {
        editarArticulo($_POST["id"], $_POST["titulo"], $_POST["cuerpo"], $_POST["fotos"], 
        $_POST["archivos"], $_POST["video"],  $_POST["fijo"],  $_POST["fechafijo"],
        $_POST["horafijo"]);
       
    } else {
        guardarArticulo($_POST["titulo"], $_POST["cuerpo"], $_POST["fotos"], 
        $_POST["archivos"], $_POST["video"],  $_POST["fijo"],  $_POST["fechafijo"],
        $_POST["horafijo"]);
        
    }
    header("location:index.php");
?>